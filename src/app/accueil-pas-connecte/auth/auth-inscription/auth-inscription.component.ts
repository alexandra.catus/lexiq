import { Component, OnInit, OnDestroy } from '@angular/core';
import {FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators} from '@angular/forms';
import { AuthService } from '../../../services/auth.service';
import { Router } from '@angular/router';
import {UsersService} from '../../../services/users.service';
import * as firebase from 'firebase';

@Component({
  selector: 'app-auth-inscription',
  templateUrl: './auth-inscription.component.html',
  styleUrls: ['./auth-inscription.component.css']
})
export class AuthInscriptionComponent implements OnInit {
  signupForm: FormGroup;
  errorMessage: string;

  constructor(private formBuilder: FormBuilder,
              private authService: AuthService,
              private router: Router) {}

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.signupForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.pattern(/[0-9a-zA-Z]{6,}/)]]
    });
  }
  onSubmit() {
    const valide = true;
    const email = this.signupForm.get('email').value;
    const password = this.signupForm.get('password').value;
    this.authService.inscriptionNewUser(email, password).then(
      () => {
        this.router.navigate(['/accueil']);
      },
      (error) => {
        this.errorMessage = error;
      }
    );
    }
}


