import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthConnectionComponent } from './auth-connection.component';

describe('AuthConnectionComponent', () => {
  let component: AuthConnectionComponent;
  let fixture: ComponentFixture<AuthConnectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuthConnectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthConnectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
