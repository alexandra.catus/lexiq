export class Question {
  public choix: string[] = [];
  resultat: boolean;

  constructor(public mot: string, public  reponse: string, public idFiche: number) {
    this.choix.push(reponse);
  }
  verif(value) {
    this.resultat = (value === this.reponse);
    return this.resultat;
  }
  reussi() {
    return this.resultat;
}

 shuffle() {
   let ctr = this.choix.length, temp, index;
// While there are elements in the array
   while (ctr > 0) {
// Pick a random index
     index = Math.floor(Math.random() * ctr);
// Decrease ctr by 1
     ctr--;
// And swap the last element with it
     temp = this.choix[ctr];
     this.choix[ctr] = this.choix[index];
     this.choix[index] = temp;
   }
 }

  toString() {
    return this.mot + '...' + this.reponse;
  }
}
