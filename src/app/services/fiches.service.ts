import { Injectable } from '@angular/core';
import {Fiche} from '../models/fiche.model';
import * as firebase from 'firebase';
import DataSnapshot = firebase.database.DataSnapshot;
import { Observable, Subject} from 'rxjs';
import {take} from 'rxjs/operators';
import {Tag} from '../models/tag.model';
import {FirebaseError} from 'firebase';

@Injectable( // {
 // providedIn: 'root'}
)
export class FichesService {
  fiches: Fiche[] = [];
  tags: Tag[] = [];
  fichesSubject = new Subject<Fiche[]>();
  tagsSubject = new Subject<Tag[]>();


  constructor() {
    this.getFiches();
    this.getTags();
  }
  // METHODES FICHES
  emitFiches() {
    this.fichesSubject.next(this.fiches);
  }

  saveFiches() {
    const uid = firebase.auth().currentUser.uid;
    firebase.database().ref('/Fiches/' + uid).set(this.fiches);
  }

  createNewFiche(newFiche: Fiche) {
    if (this.fiches.filter(x => x.mot === newFiche.mot).length === 0) {
      this.fiches.push(newFiche);
      this.saveFiches();
      this.emitFiches();
    }
  }

  // METHODES TAGS

  emitTags() {
    this.tagsSubject.next(this.tags);
  }
  saveTags() {
    const uid = firebase.auth().currentUser.uid;
    firebase.database().ref('/Tags/' + uid).set(this.tags);
  }

  createNewTags(newFiche: Fiche) {
    for (const t of newFiche.tags) {
      t.selected = false;
      this.saveTags();
      this.emitTags();
    }
  }

  removeFiche(fiche: Fiche) {
    const ficheIndexToRemove = this.fiches.findIndex(
      (ficheEl) => {
        if (ficheEl === fiche) {
          return true;
        }
      }
    );
    this.fiches.splice(ficheIndexToRemove, 1);
    this.saveFiches();
    this.emitFiches();
  }



  getFiches() {
    const uid = firebase.auth().currentUser.uid;
    firebase.database().ref('/Fiches/' + uid)
      .on('value', (data: DataSnapshot) => {
          this.fiches = data.val() ? data.val() : [];
          this.emitFiches();
        }
      );
  }

  initializeFiches(cherchee: boolean) {
    for (const f of this.fiches) {
      f.cherchee = cherchee;
      this.saveFiches();
      this.emitFiches();
    }
  }

  initializeTags(select: boolean) {
    this.tags.forEach(x => x.selected = select);
    this.saveTags();
    this.emitTags();
    }


  getTags() {
    const uid = firebase.auth().currentUser.uid;
    firebase.database().ref('/Tags/' + uid)
      .on('value', (data: DataSnapshot) => {
          this.tags = data.val() ? data.val() : [];
          this.emitTags();
        }
      );

  }

  /*getSingleFiche(mot: string) {
    const uid = firebase.auth().currentUser.uid;
    return new Promise(
      (resolve, reject) => {
        firebase.database().ref('/Fiches/' + uid + '/' + mot).once('value').then(
          (data: DataSnapshot) => {
            resolve(data.val());
          }, (error) => {
            reject(error);
          }
        );
      }
    );
  }*/

  getSingleFiche(mot: string) {
    const uid = firebase.auth().currentUser.uid;
    return new Promise(
      (resolve, reject) => {
        firebase.database().ref('/Fiches/' + uid + '/' + mot).once('value').then(
          (data: DataSnapshot) => {
            resolve(data.val());
          }, (error) => {
            reject(error);
          }
        );
      }
    );
  }

  addStatsFiches(mot, reussi ) {
    for (const fiche of this.fiches) {
      if (fiche.mot.toLowerCase() === mot.toLowerCase()) {
        fiche.nbQuizs++;
        if (reussi) {fiche.nbReussis++; }
        fiche.score = (fiche.nbReussis / fiche.nbQuizs);
        this.saveFiches();
        this.emitFiches();
      }
    }
  }
}
