import {Component, Input, OnDestroy, OnInit, Injectable} from '@angular/core';
import {Fiche} from '../models/fiche.model';
// import User from '../models/user.model';
import {Subscription} from 'rxjs';
import {FichesService} from '../services/fiches.service';
import {listener} from '@angular/core/src/render3/instructions';
import {Tag} from '../models/tag.model';

@Component({
  selector: 'app-fiches',
  templateUrl: './fiches.component.html',
  styleUrls: ['./fiches.component.css']
})
@Injectable()
export class FichesComponent implements OnInit, OnDestroy {
  @Input() champRecherche: string;
  fiches: Fiche[];
  fichesSubscription: Subscription;
  selectedFiche: Fiche;
  listeTagsRecherche: Tag[] = [];
  listeFicheCherchee: Fiche[];
  OrderMot: Boolean;
  OrderScore: Boolean;

  constructor (private fichesService: FichesService /*private router: Router*/) {
  }


  onTagsChangedRecherche(newliste: Tag[]) {
    this.listeTagsRecherche = newliste;
    this.onActualiser();
  }

  onFicheChange() {
    this.fichesService.saveFiches();
  }

  onFicheDelete(x: Fiche) {
    this.fichesService.removeFiche(x);
  }

  sortByMot() {
    if (this.OrderMot) {
    this.fiches.sort(function(a, b) {
      if (a.mot < b.mot) { return -1; }
      if (a.mot > b.mot) { return 1; }
      return 0;
    });
    } else {
      this.fiches.sort(function(a, b) {
        if (a.mot > b.mot) { return -1; }
        if (a.mot < b.mot) { return 1; }
        return 0;
      });
    }
    this.OrderMot = !this.OrderMot;
    this.OrderScore = false;
    }

  sortByScore() {
    if (this.OrderScore) {
    this.fiches.sort(function(a, b) {
      if (a.score < b.score) { return -1; }
      if (a.score > b.score) { return 1; }
      return 0;
    });
    } else {
      this.fiches.sort(function(a, b) {
        if (a.score > b.score) { return -1; }
        if (a.score < b.score) { return 1; }
        return 0;
    });
  }
    this.OrderScore = !this.OrderScore;
    this.OrderMot = false;
  }


  ngOnInit() {
    this.fichesSubscription = this.fichesService.fichesSubject.subscribe(
      (fiches: Fiche[]) => {
        this.fiches = fiches;
      }
    );
    this.fichesService.emitFiches();
    this.champRecherche = '';
    this.onActualiser();
    this.OrderMot = false;
    this.OrderScore = false;
  }

  ngOnDestroy() {
    this.fichesSubscription.unsubscribe();
  }


  onActualiser() {
    let motCherche = true;
    let tagCherche = true;
    for (const fiche of this.fiches) {
      motCherche = fiche.mot.toLowerCase().search(this.champRecherche.toLowerCase()) !== -1;
      if (this.listeTagsRecherche.length > 0) {
        tagCherche = this.hasTag(fiche);
      }
      fiche.cherchee = (motCherche && tagCherche);
    }
  }

  hasTag(fiche) {
    let recherche = true;
    const listeTagsStr = [];
    if (typeof fiche.tags !== 'undefined') {
    for (const tag of fiche.tags) {
      listeTagsStr.push(tag.name.toLowerCase());
      }
      for (const tagUser of this.listeTagsRecherche) {
        if (!listeTagsStr.includes(tagUser.name.toLowerCase())) {
          recherche = false;
        }
      }
    } else {recherche = false; }
        return recherche;
  }


}
