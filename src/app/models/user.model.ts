export class User {
  nom: string;
  email: string;
  urlPhoto: string;
  nbFiches: number;
  nbReussis: number;
  nbQuizs: number;
  visites: number[];

  constructor() {
    this.email = '';
    this.nom = '';
    this.urlPhoto = '';
    this.nbFiches = 0;
    this.nbReussis = 0;
    this.nbQuizs = 0;
    this.visites = [Date.now()];
  }

  addVisite() {
    this.visites.push(Date.now());
  }
}
