import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import DataSnapshot = firebase.database.DataSnapshot;
import {Subject} from 'rxjs';
import {Fiche} from '../../../../Xiq/src/app/models/fiche.model';
import {Question} from '../../../../Xiq/src/app/models/question.model';

@Injectable({
  providedIn: 'root'
})
export class QuizService {
  quiz =
    {
      // Pour l'instant mise de côté de l'option difficulté
    };

  constructor() { }

  creerQuizz(fiches: Fiche[], nbQuestions: number) {
    const questions: Question[] = [];
    const indexs: number[] = [];
    let index: number;
    let index2: number;
    const max: number = fiches.length;

    // Trouver 5 index au hasard parmi ceux qu'on a déjà
    const arr: number[] = [];
    let randomnumber;
    while (arr.length < nbQuestions) {
      randomnumber = Math.floor(Math.random() * max);
      if (arr.indexOf(randomnumber) > -1) { continue; }
      arr[arr.length] = randomnumber;
      const question = new Question(fiches[randomnumber].mot, fiches[randomnumber].definition, randomnumber);
      questions.push(question);
    }

    for (let i = 0 ; i < nbQuestions ; i++) {
      index = Math.floor(Math.random() * nbQuestions);
      index2 = Math.floor(Math.random() * nbQuestions);
      if (index !== i && index2 !== i && index !== index2) {
        questions[i].choix.push(questions[index].reponse);
        questions[i].choix.push(questions[index2].reponse);
      } else {
        i--;
      }

    }
    for (const i of questions) {
      i.shuffle();
    }
    return questions;
}


}
