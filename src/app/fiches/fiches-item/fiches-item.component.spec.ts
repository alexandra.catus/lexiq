import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FichesItemComponent } from './fiches-item.component';

describe('FichesItemComponent', () => {
  let component: FichesItemComponent;
  let fixture: ComponentFixture<FichesItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FichesItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FichesItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
