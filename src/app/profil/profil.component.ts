import { Component, OnInit, OnDestroy } from '@angular/core';
import {AuthService} from '../services/auth.service';
import {UsersService} from '../services/users.service';
import {Router} from '@angular/router';
import {User} from '../models/user.model';
import {Fiche} from '../models/fiche.model';
import {Subscription} from 'rxjs';
import {FichesService} from '../services/fiches.service';

@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.css']
})
export class ProfilComponent implements OnInit, OnDestroy {
  user: User;
  today: number;
  fiches: Fiche[];
  userSubscription: Subscription;
  fichesSubscription: Subscription;

  constructor(private usersService: UsersService,
              private fichesService: FichesService,
              private authService: AuthService,
              private router: Router) {
  }
  getInfo() {
    let reussite = 0;
    if (this.fiches.length > 0) {
      reussite = Math.round((this.user.nbReussis / this.fiches.length) * 100); }
  const info = [
    {nombre: Math.round((this.user.visites[0] - this.today) / (24 * 3600 * 1000)), info: 'jours de login'},
    {nombre: this.user.visites.length, info: 'visites'},
    {nombre: this.fiches.length, info: 'fiches'},
    {nombre: this.user.nbQuizs, info: 'questions'},
    {nombre: reussite, info: '% de réussite'},
    {nombre: this.fiches.filter(x => x.score > 85).length, info: 'mots maitrisés'}
    ];
  return info;
  }

  ngOnDestroy() {
    this.userSubscription.unsubscribe();
    this.fichesSubscription.unsubscribe();
  }

  ngOnInit() {
    this.userSubscription = this.usersService.userSubject.subscribe(
      (user: User) => {
        this.user = user;
      }
    );
    this.usersService.emitUser();

    this.fichesSubscription = this.fichesService.fichesSubject.subscribe(
      (fiches: Fiche[]) => {
        this.fiches = fiches;
      }
    );
    this.fichesService.emitFiches();
    this.today = Date.now();
  }

  deconnection() {
    this.authService.signOutUser().then(() => {
      this.userSubscription.unsubscribe();
      this.fichesSubscription.unsubscribe();
      this.router.navigate(['/auth']);
    });
  }


}
