import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-four-oh-four',
  templateUrl: './four-oh-four.component.html',
  styleUrls: ['./four-oh-four.component.css']
})
export class FourOhFourComponent implements OnInit {
  @Input() number: number[]=[];
  constructor() { }

  ngOnInit() {
    for (let x = 0; x <= 404; x ++) {
      this.number.push(x);
    }
  }

}
