import {Component, OnInit, OnDestroy} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {QuizService } from '../services/quiz.service';
import {Question} from '../models/question.model';
import {Fiche} from '../models/fiche.model';
import {FichesService} from '../services/fiches.service';
import {UsersService} from '../services/users.service';
import {Subscription} from 'rxjs';
import {User} from '../models/user.model';
import {Tag} from '../models/tag.model';

@Component({
  selector: 'app-quiz',
  templateUrl: './quiz.component.html',
  styleUrls: ['./quiz.component.css']
})
export class QuizComponent implements OnInit, OnDestroy {
  errorMessageOption: string;
  indexCourant: number;
  quizzReady: Boolean;
  quizzFini: Boolean;
  questions: Question[] = [];
  fiches: Fiche[] = [];
  choixActuel = String;
  choix: String[] = [];
  user: User;
  userSubscription: Subscription;
  fichesSubscription: Subscription;
  quizOptionsForm: FormGroup;
  quizQuestionsForm: FormGroup;
  messageErreur: String;
  nbQuestions: number;
  listeTagsRecherche: Tag[] = [];
  fichesQuizz: Fiche[] = [];

  constructor(private formBuilder: FormBuilder,
              private fichesService: FichesService,
              private quizService: QuizService,
              private usersService: UsersService) {}

  onTagsChangedRecherche(newliste) {
      this.listeTagsRecherche = newliste;
      this.hasTag();
  }

  selectedfiches() {
    return this.fiches.filter(x => x.cherchee === true);
  }



  hasTag() {
    let listeTagsStr = [];
    const listeFichesProv = [];
    let recherche;
      for (const fiche of this.fiches) {
        listeTagsStr = [];
        recherche = true;
        if (typeof this.listeTagsRecherche !== 'undefined' && this.listeTagsRecherche.length > 0) {
          if (typeof fiche.tags !== 'undefined') {
          for (const tag of fiche.tags) {
          listeTagsStr.push(tag.name.toLowerCase());
          }
        for (const tagUser of this.listeTagsRecherche) {
          if (!listeTagsStr.includes(tagUser.name.toLowerCase())) {
            recherche = false;
        }
        }

      } else {recherche = false; } }
        if (recherche) {
          listeFichesProv.push(fiche);
        }

      }
      return listeFichesProv;
      }

  initForm() {
    this.quizQuestionsForm = this.formBuilder.group({
      radioReponse: ['']
    });
    this.quizOptionsForm = this.formBuilder.group({
      nbQuestions: [5, [Validators.required]]
    });
  }

  onSelectionChange(value) {
    this.choixActuel = value;
  }
  ngOnInit() {
    this.initForm();
    this.userSubscription = this.usersService.userSubject.subscribe(
      (user: User) => {
        this.user = user;
      }
    );
    this.usersService.emitUser();
    this.fichesSubscription = this.fichesService.fichesSubject.subscribe(
      (fiches: Fiche[]) => {
        this.fiches = fiches;
      }
    );
    this.fichesService.emitFiches();
    this.indexCourant = 0;
    this.quizzReady = false;
    this.quizzFini = false;
    this.questions = [];
  }
  onSubmitOptions() {
    const formValue = this.quizOptionsForm.value;
    this.nbQuestions = formValue['nbQuestions'];
    if (this.hasTag().length >= this.nbQuestions) {
      this.quizzReady = true;
      this.questions = this.quizService.creerQuizz(this.hasTag(), this.nbQuestions);
      this.indexCourant = 0;
      this.nextQuestion();
    } else {
      this.errorMessageOption = 'Pas assez de fiches pour créer un quizz!';
    }
  }

  nextQuestion() {
    this.choix = this.questions[this.indexCourant].choix ;
  }
  //
  onSubmitQuestion() {
    const questionActuelle = this.questions[this.indexCourant];
    questionActuelle.verif(this.choixActuel);
    this.fichesService.addStatsFiches(questionActuelle.mot, questionActuelle.reussi());
    this.usersService.addStats(questionActuelle.reussi());
    this.quizQuestionsForm.reset();
    if (this.resteQuestions()) {
      this.indexCourant++ ;
      this.nextQuestion();
    } else {
      this.quizzFini = true;
    }
  }

  resteQuestions() {
    return (this.indexCourant + 1) < this.nbQuestions;
  }
  isReady() {
    return this.quizzReady;
  }

  isFini() {
    return this.quizzFini;
  }


  ngOnDestroy() {
    this.userSubscription.unsubscribe();
    this.fichesSubscription.unsubscribe();
  }
}
