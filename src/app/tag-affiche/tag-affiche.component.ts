import { Component, OnInit, Input } from '@angular/core';
import {Tag} from '../models/tag.model';

@Component({
  selector: 'app-tag-affiche',
  templateUrl: './tag-affiche.component.html',
  styleUrls: ['./tag-affiche.component.css']
})
export class TagAfficheComponent implements OnInit {
  @Input() tags: Tag[];
  constructor() { }
  ngOnInit() {}
}
