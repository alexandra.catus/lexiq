import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccueilPasConnecteComponent } from './accueil-pas-connecte.component';
import { AuthComponent } from '../accueil-pas-connecte/auth/auth.component';
import {AuthConnectionComponent} from './auth/auth-connection/auth-connection.component';
import {AuthInscriptionComponent} from './auth/auth-inscription/auth-inscription.component';

describe('AccueilPasConnecteComponent', () => {
  let component: AccueilPasConnecteComponent;
  let fixture: ComponentFixture<AccueilPasConnecteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AccueilPasConnecteComponent,
        AuthComponent,
      AuthConnectionComponent,
      AuthInscriptionComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccueilPasConnecteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
