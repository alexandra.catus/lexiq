import { Injectable } from '@angular/core';
import * as firebase from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class TagsService {

  uid = firebase.auth().currentUser.uid;
  cheminDataTag = firebase.database().ref('tags/' + this.uid);

  constructor() { }

  ajouterTags(tag: string) {
    this.cheminDataTag.set({
      tags: tag
    });
  }

  RetirerTags(tag: string) {
    const uid = firebase.auth().currentUser.uid;
    const cheminDataTag = firebase.database().ref('tags/' + uid);
    cheminDataTag.set({
      tags: tag
    });
  }
}
