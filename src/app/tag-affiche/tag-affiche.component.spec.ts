import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TagAfficheComponent } from './tag-affiche.component';

describe('TagAfficheComponent', () => {
  let component: TagAfficheComponent;
  let fixture: ComponentFixture<TagAfficheComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TagAfficheComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TagAfficheComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
