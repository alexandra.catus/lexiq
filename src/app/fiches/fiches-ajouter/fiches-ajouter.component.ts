import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Fiche} from '../../models/fiche.model';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {FichesService} from '../../services/fiches.service';
import {Router} from '@angular/router';
import {NullAstVisitor} from '@angular/compiler';
import {Tag} from '../../models/tag.model';

@Component({
  selector: 'app-fiches-ajouter',
  templateUrl: './fiches-ajouter.component.html',
  styleUrls: ['./fiches-ajouter.component.css']
})
export class FichesAjouterComponent implements OnInit {
  ajoutFiche: FormGroup;
  errorMessage: string;
  listeTagsFiche: Tag[] = [];
  TagsFiche: Tag[] = [];
  constructor(private formBuilder: FormBuilder,
              private fichesService: FichesService,
              private router: Router
  ) {}

  onTagsChangedAjout(newliste: Tag[]) {
    this.TagsFiche = newliste;
  }
  ngOnInit() {
    this.ajoutFiche = this.formBuilder.group({
      mot: ['', [Validators.required]],
      def: ['', [Validators.required]]
    });
  }


  onSubmit() {
    const mot = this.ajoutFiche.get('mot').value;
    const def = this.ajoutFiche.get('def').value;
    const newFiche = new Fiche(mot, def);
    this.TagsFiche.forEach(x => x.selected = false);
    newFiche.tags = this.TagsFiche;
    this.fichesService.createNewFiche(newFiche);
    this.fichesService.createNewTags(newFiche);
    this.ajoutFiche.reset();
  }
}
