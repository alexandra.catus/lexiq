import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule  } from '@angular/forms';
import { AuthService } from '../../../services/auth.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-auth-connection',
  templateUrl: './auth-connection.component.html',
  styleUrls: ['./auth-connection.component.css']
})
export class AuthConnectionComponent implements OnInit {
  connectionForm: FormGroup;
  errorMessage: string;

  constructor(private formBuilder: FormBuilder,
              private authService: AuthService,
              private router: Router) { }
  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.connectionForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.pattern(/[0-9a-zA-Z]{6,}/)]]
    });
  }
  onSubmit() {
    const email = this.connectionForm.get('email').value;
    const password = this.connectionForm.get('password').value;
    this.authService.connectionUser(email, password).then(
      () => {
         this.router.navigate(['/accueil']);
      },
      (error) => {
        this.errorMessage = error;
      }
    );
  }

}
