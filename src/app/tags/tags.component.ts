import {Component, Input, OnDestroy, Output, OnInit, EventEmitter} from '@angular/core';
import { Tag } from '../models/tag.model';
import {FichesService} from '../services/fiches.service';
import {FormGroup} from '@angular/forms';
import {Fiche} from '../models/fiche.model';
import { Subscription} from 'rxjs';
import {forEach} from '@angular/router/src/utils/collection';

@Component({
  selector: 'app-tags',
  templateUrl: './tags.component.html',
  styleUrls: ['./tags.component.css']
})

export class TagsComponent implements OnInit, OnDestroy {
  @Input() tags: Tag[];
  @Input() modif: boolean;
  @Output() tagsChanged = new EventEmitter<Tag[]>();
  strTagsUser: Tag[];
  tagsSubscription: Subscription;

  tagsChange() {
    this.tagsChanged.emit(this.filterSelected());
  }

  filterSelected() {
    const temp_list: Tag[] = [];
    for (const tag of this.strTagsUser.filter(x => x.selected === true)) {
          temp_list.push(tag);
        }
    return temp_list;
}

  changeState(tag: Tag) {
    tag.selected = !tag.selected;
    this.tagsChange();
  }


  onEnter(value: string): void {
    if (value !== '' ) {
      if (this.strTagsUser.filter(x => x.name === value).length === 0) {
      this.strTagsUser.push({name: value, selected: true});
    }
      (<HTMLInputElement>document.getElementById('tagInput')).value = '';
      this.tagsChange();
    }
  }

  constructor(private fichesService: FichesService) {
  }

  ngOnDestroy() {
    this.tagsSubscription.unsubscribe();
  }

  ngOnInit() {
    this.tagsSubscription = this.fichesService.tagsSubject.subscribe(
      (tags: Tag[]) => {
        this.strTagsUser = tags;
      }
    );
    this.fichesService.emitTags();
  }

}

