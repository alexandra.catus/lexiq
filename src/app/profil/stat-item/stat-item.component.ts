import { Input, Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-stat-item',
  templateUrl: './stat-item.component.html',
  styleUrls: ['./stat-item.component.css']
})
export class StatItemComponent implements OnInit {
  @Input() stat;
  constructor() { }

  ngOnInit() {
  }

}
