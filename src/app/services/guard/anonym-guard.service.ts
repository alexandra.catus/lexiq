import { Injectable} from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import {Observable} from 'rxjs';
import * as firebase from 'firebase';

@Injectable()

export class AnonymGuardService implements CanActivate {
  constructor(private router: Router) { }
  canActivate(): Observable<boolean> | Promise<boolean> | boolean {
    return new Promise(
      (resolve, reject) => {
        firebase.auth().onAuthStateChanged(
          (user) => {
            if (user) {
              this.router.navigate(['/profil']);
              resolve(false);
            } else {
              resolve(true);
            }
          }
        );
      }
    );
  }

}
