import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import {reject} from 'q';
import {User} from '../models/user.model';
import DataSnapshot = firebase.database.DataSnapshot;
import {Subject} from 'rxjs';
import {Fiche} from '../models/fiche.model';
// @Todo : injecter la base de donnée & l'uid une seule fois :)
@Injectable({
  providedIn: 'root'
})
export class UsersService {
  user: User;
  userSubject = new Subject<User>();


  emitUser() {
    this.userSubject.next(this.user);
  }

  constructor() {
    this.getUser();
  }

  getUser() {
      const uid = firebase.auth().currentUser.uid;
      firebase.database().ref('/Users/' + uid).on('value', (data: DataSnapshot) => {
        this.user = data.val() ? data.val() : new User();
        console.log(data.val());
        this.emitUser();
      }
      );
    }

  saveUser() {
    const uid = firebase.auth().currentUser.uid;
    firebase.database().ref('/Users/' + uid).set(this.user);
  }


  addStats(reussis: boolean) {
    this.user.nbQuizs++;
    if (reussis) {this.user.nbReussis++; }
    this.saveUser();
    this.emitUser();
  }


}
