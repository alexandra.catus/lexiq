import { Component } from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Xiq';
  constructor() {
    const config = {
      apiKey: 'AIzaSyA1xSkyzK28lZjtSzJxR8CrBhhYce09zMU',
      authDomain: 'lexico-eb1eb.firebaseapp.com',
      databaseURL: 'https://lexico-eb1eb.firebaseio.com',
      projectId: 'lexico-eb1eb',
      storageBucket: 'lexico-eb1eb.appspot.com',
      messagingSenderId: '705913777622'
    };
    firebase.initializeApp(config);
  }
}
