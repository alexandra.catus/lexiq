import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FichesAjouterComponent } from './fiches-ajouter.component';

describe('FichesAjouterComponent', () => {
  let component: FichesAjouterComponent;
  let fixture: ComponentFixture<FichesAjouterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FichesAjouterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FichesAjouterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
