import {Tag} from './tag.model';

export class Fiche {
  cherchee: boolean;
  tags: Tag[];
  nbQuizs: number;
  nbReussis: number;
  score: number;

  constructor(public mot: string, public definition: string) {
    this.cherchee = true;
    this.tags = [];
    this.nbQuizs = 0;
    this.nbReussis = 0;
    this.nbReussis = 0;
    this.score = 0;
  }


  setScore() {
    if (this.nbQuizs > 0) {
      this.score = (this.nbReussis / this.nbQuizs) * 100;
    }
  }

}
