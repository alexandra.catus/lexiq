import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import {reject} from 'q';
import {errorComparator} from 'tslint/lib/verify/lintError';
import {User} from '../models/user.model';
import DataSnapshot = firebase.database.DataSnapshot;
import {Fiche} from '../models/fiche.model';
import {UsersService} from './users.service';
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor() {
  }

  signOutUser() {
    return new Promise( (resolve) => {
      firebase.auth().signOut().then(() => {
        const message = 'Vous êtes bien deconnecté';
        resolve();
        }, (error) => {
        reject(error);
        }
      );
    }
    );
  }

  inscriptionNewUser(email: string, password: string) {
    return new Promise(
      (resolve, reject) => {
        firebase.auth().createUserWithEmailAndPassword(email, password).then(
          () => {
            resolve();
          },
          (error) => {
            reject('Erreur: Cette addresse est déjà enregistrée');
          }
        );
      }
    );
  }

  connectionUser(email: string, password: string) {
    return new Promise(
      (resolve, reject) => {
        // @Todo: aller chercher les infos de l'utilisateur
        firebase.auth().signInWithEmailAndPassword(email, password).then(
          () => {
            resolve();
          },
          (error) => {
            reject(error);
          }
        );
      }
    );
  }


}
