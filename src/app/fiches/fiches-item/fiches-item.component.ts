import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';
import {Fiche} from '../../models/fiche.model';
import {Tag} from '../../models/tag.model';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-fiches-item',
  templateUrl: './fiches-item.component.html',
  styleUrls: ['./fiches-item.component.css']
})

export class FichesItemComponent implements OnInit {
  @Input() fiche: Fiche;
  @Output() ficheChange = new EventEmitter();
  @Output() ficheDelete = new EventEmitter<Fiche>();
  modifFiche: FormGroup;
  tagsProv: Tag[];

  isCherchee() {
    return this.fiche.cherchee;
  }

  confEffaceFiche() {
    this.ficheDelete.emit(this.fiche);
  }

  confModfifFiche() {
    this.fiche.mot = (this.modifFiche.get('motModif').value);
    this.fiche.definition = (this.modifFiche.get('defModif').value);
    this.ficheChange.emit();
  }

  onTagsChangedModif(tags: Tag[]) {
    this.tagsProv = tags;
  }

  constructor(private formBuilder: FormBuilder) {
    this.ficheDelete.emit();
  }
  ngOnInit() {
    this.modifFiche = this.formBuilder.group({
      motModif: [this.fiche.mot, [Validators.required]],
      defModif: [this.fiche.definition, [Validators.required]]
    });
    this.tagsProv = this.fiche.tags;
  }

  getScore() {
    if (this.fiche.nbQuizs > 0) {
      return (this.fiche.nbReussis / this.fiche.nbQuizs) * 100;
    } else {
      return 0;
    }
  }

}
