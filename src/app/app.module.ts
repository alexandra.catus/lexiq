import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { TagsComponent } from './tags/tags.component';
import { AuthComponent } from './accueil-pas-connecte/auth/auth.component';
import { ProfilComponent } from './profil/profil.component';
import { FichesAjouterComponent } from './fiches/fiches-ajouter/fiches-ajouter.component';
import { FichesItemComponent } from './fiches/fiches-item/fiches-item.component';
import { AuthInscriptionComponent } from './accueil-pas-connecte/auth/auth-inscription/auth-inscription.component';
import { AuthConnectionComponent } from './accueil-pas-connecte/auth/auth-connection/auth-connection.component';
import { AccueilConnecteComponent } from './accueil-connecte/accueil-connecte.component';
import { AccueilPasConnecteComponent } from './accueil-pas-connecte/accueil-pas-connecte.component';
import { AuthService} from './services/auth.service';
import { FichesComponent } from './fiches/fiches.component';
import {FichesService} from './services/fiches.service';

import { Routes } from '@angular/router';
import { FourOhFourComponent } from './four-oh-four/four-oh-four.component';
import {AuthGuardService} from './services/guard/auth-guard.service';
import {AnonymGuardService} from './services/guard/anonym-guard.service';
import { QuizComponent } from './quiz/quiz.component';
import {UsersService} from './services/users.service';
import { TagAfficheComponent } from './tag-affiche/tag-affiche.component';
import { StatItemComponent } from './profil/stat-item/stat-item.component';

const appRoutes: Routes = [
  { path: 'accueil', canActivate: [AuthGuardService], component: AccueilConnecteComponent},
  { path: 'fiches', canActivate: [AuthGuardService], component: FichesComponent},
  { path: 'profil', canActivate: [AuthGuardService], component: ProfilComponent },
  { path: 'auth', canActivate: [AnonymGuardService], component: AccueilPasConnecteComponent },
  { path: 'quiz', canActivate: [AuthGuardService], component: QuizComponent },
  { path: 'not-found', component: FourOhFourComponent},
  { path: '', canActivate: [AuthGuardService], component: FichesComponent },
  { path: '**', redirectTo: 'not-found' }
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    TagsComponent,
    AuthComponent,
    ProfilComponent,
    FichesAjouterComponent,
    FichesItemComponent,
    AuthInscriptionComponent,
    AuthConnectionComponent,
    AccueilConnecteComponent,
    AccueilPasConnecteComponent,
    FichesComponent,
    FourOhFourComponent,
    QuizComponent,
    TagAfficheComponent,
    StatItemComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [
    AuthService,
    AuthGuardService,
    AnonymGuardService,
    FichesService,
    UsersService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
