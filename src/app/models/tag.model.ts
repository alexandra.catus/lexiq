export class Tag {
  public selected: boolean;
  constructor( public name: string) {
    this.selected = true;
  }
}
