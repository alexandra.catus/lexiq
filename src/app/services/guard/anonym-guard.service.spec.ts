import { TestBed } from '@angular/core/testing';

import { AnonymGuardService } from './anonym-guard.service';

describe('AnonymGuardService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AnonymGuardService = TestBed.get(AnonymGuardService);
    expect(service).toBeTruthy();
  });
});
